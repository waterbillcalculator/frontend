import { useState } from 'react';

import { Form,Button } from 'react-bootstrap';

function App() {

  const [account,setAccount] = useState('');

  const [waterCons,setWaterCons] = useState(0);

  const [accountDetails,setAccountDetails] = useState({
    accountNo: null,
    firstName : null,
    lastName : null,
    email : null,
    address : null,
    mobileNo : null
  });

  const [ bill,setBill] = useState(0)

  
  function Bill(e){

    e.preventDefault();
//USER DETAILS
    fetch('http://localhost:4000/',{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        accountNo: account
      })
    })
    .then(res => res.json())
    .then(data => {
      setAccountDetails({
        accountNo: data.accountNo,
        firstName : data.firstName,
        lastName : data.lastName,
        email : data.email,
        address : data.address,
        mobileNo : data.mobileNo
      });
    })


//WATER BILL COMPUTATION
    let number = 0;
    let total = 0;

    if(waterCons > 50) {
      number = waterCons - 50
      total = (number * 18.95) + 767

      return setBill(total)
    }
    if(waterCons > 40) {
      number = waterCons - 40
      total = (number * 17.7) + 590

      return setBill(total)
    }
    if(waterCons > 30) {
      number = waterCons - 30
      total = (number * 16.6) + 424

      return setBill(total)
    }
    if(waterCons > 20) {
      number = waterCons - 20
      total = (number * 15.5) + 269

      return setBill(total)
    }
    if(waterCons > 10) {
      number = waterCons - 10
      total = (number * 14.4) + 125

      return setBill(total)
    }
    if(waterCons > 0) {
      number = waterCons
      total = (number * 12.5)

      return setBill(total)
    }
  }
    

  return (
    <>
      <h1>Water bill calculator</h1>

      <Form onSubmit={e=> Bill(e)}>
      <Form.Group>
					<Form.Label>Account Number:</Form.Label>
					<Form.Control type="text" placeholder="xxx-xx-xxx" required value={account} onChange={e => {setAccount(e.target.value)}} />
			</Form.Group>

      <Form.Check
        inline
        label="regular"
        name="group1"
        type="radio"
        value="false"
      />
      <Form.Check
        inline
        label="senior"
        name="group1"
        type="radio"
        value="true"
      />

      <Form.Group>
					<Form.Label>Water Consumption(in cubic meters):</Form.Label>
					<Form.Control type="number" required value={waterCons} onChange={e => {setWaterCons(e.target.value)}} />
			</Form.Group>
      <Button variant="primary" type="submit" className="my-5">Calculate</Button>
      </Form>
      <p>account number: {accountDetails.accountNo}</p>
      <p>first name: {accountDetails.firstName}</p>
      <p>last name: {accountDetails.lastName}</p>
      <p>email: {accountDetails.email}</p>
      <p>address: {accountDetails.address}</p>
      <p>Mobile number: {accountDetails.mobileNo}</p>
      <h3>Bill: {bill}</h3>
    </>
  );
}

export default App;
